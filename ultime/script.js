$(function(){
	$('.hamburger-menu').on('click', function(){
		$('.toggle').toggleClass('open');
		$('.nav-list').toggleClass('open');
	});

	AOS.init({
		easing: 'ease',
		duration: 1000,
	});
});

/*$(document).ready( function () {

	$( " #mycarousel").carousel({ interval: 2000, pause: "hover"});

	$(".item1").click(function(){
		$("#mycarousel").carousel(0);
	});

	$(".item2").click(function(){
		$("#mycarousel").carousel(1);
	});


	$(".carousel-control-prev").click(function(){
		$("#mycarousel").carousel("prev");
	});
	$(".carousel-control-next").click(function(){
		$("#mycarousel").carousel("next");
	});
} );*/


