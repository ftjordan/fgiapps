<!DOCTYPE html>

<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="accueilstyle.css">
	<link rel="stylesheet" type="text/css" href="aos/dist/aos.css">
	<!--<link rel="stylesheet" type="text/css" href="style.css">-->

	
	
	
	<title>Accueil</title>
</head>
<body>

    <header>
       <?php include("pages/header.php") ?>
    </header>
<!-- /header -->

    <div class="sections home text-center">
    	<div class="overlay">
    		<div class="container">
    		   <div class="home-content">
    		   	 <h3 class="home-title"> Bienvenue sur FGIApps</h3>
    		     <p class="lead home-desc">Faculté de genie industriel</p>
    		     <button style="margin: 10px;" class="btn button ">s'inscrire</button>
    		     <button class="btn button">se connecter</button>
    		   </div>
    	    </div>
    	</div>
    </div>



<!-- connexion -->
<div class="modal" id="mymodel">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						<h3 class="text-primary">Connectez-vous</h3>	
						<button type="button" class="close" data-dismiss="modal">&times;</button>	
					</div>
					<div class="modal-body">
						<form>
							<div class="form-group">
								<label><i class="fa fa-user fa-2x"></i>Nom utilisateur:</label>
								<input type="text" name="" class="form-control">
							</div>
							<div class="form-group">
								<label><i class="fa fa-lock fa-2x"></i>mot de passe:</label>
								<input type="Password" name="" class="form-control">
							</div>
							<div class="form-group">
								<label><i class="fa fa-envelope fa-2x"></i>E-mail:</label>
								<input type="text" name="" class="form-control">
							</div>
						</form>
						
					</div>
					<div class="moda-footer justify-content-center">
						<button class="btn btn-danger justify-content-center" data-dismiss="modal">SignUp</button>
					</div>
				</div>
			</div>
		</div>

<!--fin -->

    <div class="sections about">
    	<div class="container">
    		<div class="section-header text-center">
    			<h2 data-aos="fade-down" class="section-title">Qui Sommes Nous?</h2>
    		    <div data-aos="fade-down" class="line"><span></span></div>
    		    <p data-aos="fade-up">un groupe d'eleve de la FGI qui met a votre disposition des cours de soutien, des pdf sofistiqués ainsi que des exercises en toutes les UE confonduent et tous les niveaux.</p>
    		</div>

          <main>
            <!-- carousel slide -->
    

            <!-- fin -->


           <div class="container" id="d1">
		      <div data-aos="fade-up" ><p style="color: turquoise; font-size: 25px;  font-family: 'Niconne', cursive; ">Ecole des ingenieurs managers</p></div>
		      <div data-aos="fade-left"><p style="color: white; font-size: 25px; font-family: 'Niconne', cursive; ">La plus grande ecole d'ingenieurie de l'afrique centrale</p></div>
		      <div data-aos="fade-left"><p style="color: white; font-size: 25px;  font-family:'Niconne', cursive; ">venez et Decouvreez la facultée de genie industriel</p></div>
		      <div data-aos="fade-left"><p style="color: white; font-size: 25px; font-family: 'Niconne', cursive; ">parcourrez les differents projets realiser par les etudiants de chez nous </p></div>
            </div> 

	
	         <div class="row">
		          <div class="col-md-6">
			          <div class="about-info">
				         <h3 data-aos="fade-down">connectez vous sur <span>l'app web de lafaculté de genie industriel</span> et trouvez tous ce dont vous avez besoin </h3>
				         <p class="about-info-desc" data-aos="fade-up">cette application est mise sous pied pour mettre a votre disposition des cours de soutien, des pdf sofistiqués ainsi que des exercises en toutes les UE confonduent et tous les niveaux. il ya egalement pour vous des tutoriels qui vous permetront de mieux apprehender le cour. Nous mettons aussi a votre disposition les notes des differents semestres et une mise a jour directe a la sortie de nouveaux resultats.</p>
				         <button class="btn button" data-target="#mymodel" data-toggle="modal" data-aos="fade-up">connectez-vous</button>
			         </div>
		         </div>
	           	 <div class="col-md-6">
	           	 	<div class="about-img" data-aos="fade-left">
	           	 		<img src="images/fgi.jpg" alt="" class="img-fluid">
	           	 	</div>
	           	 </div>
	         </div>	

         </main>

    </div>
   </div>

   <div class="sections services">
   	<div class="container">
   		<div class="section-header text-center">
    			<h2 data-aos="fade-up" class="section-title">ce que nous faisons?</h2>
    		    <div data-aos="fade-up" class="line"><span></span></div>
    		    <p>un groupe d'eleve de la FGI qui met a votre disposition des cours de soutien, des pdf sofistiqués ainsi que des exercises en toutes les UE confonduent et tous les niveaux.</p>
    	</div>

    	<div class="row">
    		<div class=" col-md-4  col-xs-12">
    			<div class="serv">
    				<a href="pages/cours.php"><i class="icon fa fa-book fa-lg"></i>
    				<h3 class="serv-title">Les Cours</h3></a>
    				<p class="serv-desc">nous vous offrons des cours simplifiés ...ceci dans le but d'une meilleure formation et faciliter la comprehension des cours a l'ecole..</p>
    			</div>
    		</div>
    		<div class="col-md-4 col-xs-12">
    			<div class="serv">
    				<a href="pages/notes.php"><i class="icon fa fa-book fa-lg"></i>
    				<h3 class="serv-title">les resultats</h3></a>
    				<p class="serv-desc">voulez vous consulter vos notes de cc ou de session normale? vous etes sur la bonne page.</p>
    			</div>
    		</div>
    		<div class=" col-md-4 col-xs-12">
    			<div class="serv">
    				<a href="#"><i class="icon fa fa-book fa-lg"></i>
    				<h3 class="serv-title">Projet Etudiants</h3></a>
    				<p class="serv-desc">nous vous offrons des cours simplifiés ...eivd</p>
    			</div>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-md-4 col-xs-12">
    			<div class="serv">
    				<a href="#"><i class="icon fa fa-book fa-lg"></i>
    				<h3 class="serv-title">les tutoriels</h3></a>
    				<p class="serv-desc">nous vous offrons des cours simplifiés ...eivd</p>
    			</div>
    		</div>
    		<div class="col-md-4 col-xs-12">
    			<div class="serv">
    				<a href="#"><i class="icon fa fa-book fa-lg"></i>
    				<h3 class="serv-title">Forum</h3></a>
    				<p class="serv-desc">nous vous offrons des cours simplifiés ...eivd</p>
    			</div>
    		</div>
    		<div class="col-md-4 col-xs-12">
    			<div class="serv">
    				<i class="icon fa fa-book fa-lg"></i>
    				<a href="#"><h3 class="serv-title">Documentation</h3></a>
    				<p class="serv-desc">nous vous offrons des cours simplifiés ...eivd</p>
    			</div>
    		</div>
    	</div>
   	</div>
   </div>
 
	 
        <footer class="menu" id="menu">
			<?php include("pages/footer.php") ?>
		</footer>

	<script type="text/javascript" src="javascript/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="javascript/bootstrap.min.js"></script>
	<script type="text/javascript" src="javascript/all.js"></script>
	<script type="text/javascript" src="javascript/script.js"></script>
	<script type="text/javascript" src="aos/dist/aos.js"></script>
</body>
</html>