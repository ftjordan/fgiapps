$(function(){

	AOS.init({
		easing: 'ease',
		duration: 1000,
	});
});

$(document).ready(function(){

	// slide toggle
	$('#icon').click(function(){
		$('.ul-list').slideToggle()
	});

	

	// stickey navbar
	$(window).scroll(function(){
		var sc = $(this).scrollTop();
		if(sc > 50) {
			$('header').addClass('stickey');
		}else{
			$('header').removeClass('stickey');
		}
	});
})



