
<!DOCTYPE html>
<html>
<head> 
	<title>Nos offres</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
	
	<link rel="stylesheet" type="text/css" href="../style/styleAbout.css">
	<link rel="stylesheet" type="text/css" href="../aos/dist/aos.css">

	
</head>
<body>

 <header>
		<?php include("pages/header.php") ?>
	</header>
	<section class="about" id="about">
			<div class="container">
				<div class="row align-items-lg-center">
					<div class="col-12 col-md-6 text-center text-md-center " data-aos="fade-right">
						<a style="cursor: pointer;" href="../pages/cours.php"><div class="section-heading mb-3">
							<h1 class="display-4">Les cours</h1></a>
						</div>
						<p>vsfjgfhjgez ygrhbkrg rkkjhrh rjhrhr hrhrfb fjrhgrg grvqkqghk grkgrhkrjg ergrehjgerj rvjhrehjrej erjhgrjh rejhgrgergrehg rejherjhrj hrvjklotgj khrggvrgfhrfg jrjgrekkerhgrvj skhrgfkkrh rhrrjrgjaaeez ydhddgdgdhdd kiftdp auue ggs jf gr yhgfnjdjdgfkf jjfhhgfh fhf</p>
					</div>
					<div class="col-12 col-md-6" data-aos="fade-left">
						<img class="img-fluid" src="../images/cours.jpeg" alt="">
					</div>
				</div>
				<div class="row align-items-lg-center">
					<div class="col-12 col-md-6 text-center text-md-center " data-aos="fade-right">
						<div class="section-heading mb-3">
							<h1 class="display-4">les Tutoriels</h1>
						</div>
						<p>ceci pour une meilleure formation et faciliter la comprehension des cours..</p>
					</div>
					<div class="col-12 col-md-6" data-aos="fade-left">
						<img class="img-fluid" src="../images/recherche.jpg" alt="">
					</div>
				</div>
				<div class="row align-items-lg-center">
					<div class="col-12 col-md-6 text-center text-md-center " data-aos="fade-right">
						<div class="section-heading mb-3">
							<a href="../pages/notes.php"><h1 class="display-4">Resultats Examens</h1></a>
						</div>
						<p>voulez vous consulter vos notes de cc ou de session normale? vous etes sur la bonne page.</p>
					</div>
					<div class="col-12 col-md-6" data-aos="fade-left">
						<img class="img-fluid" src="../images/notes.png" alt="">
					</div>
				</div>
				<div class="row align-items-lg-center">
					<div class="col-12 col-md-6 text-center text-md-center " data-aos="fade-right">
						<div class="section-heading mb-3">
							<h1 class="display-4">Projet Etudiants</h1>
						</div>
						<p>vsfjgfhjgez ygrhbkrg rkkjhrh rjhrhr hrhrfb fjrhgrg grvqkqghk grkgrhkrjg ergrehjgerj rvjhrehjrej erjhgrjh rejhgrgergrehg rejherjhrj hrvjklotgj khrggvrgfhrfg jrjgrekkerhgrvj skhrgfkkrh rhrrjrgjaaeez ydhddgdgdhdd kiftdp auue ggs jf gr yhgfnjdjdgfkf jjfhhgfh fhf</p>
					</div>
					<div class="col-12 col-md-6" data-aos="fade-left">
						<img class="img-fluid" src="../images/drone.jpg" alt="">
					</div>
				</div>
			</div>
		</section>
		<section class="menu" id="menu">
			<?php include("../pages/footer.php") ?>
		</section>
	</main>


<script type="text/javascript" src="../javascript/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="../javascript/bootstrap.min.js"></script>
	<script type="text/javascript" src="../javascript/popper.min.js"></script>
	<script type="text/javascript" src="../javascript/all.js"></script>
	<script type="text/javascript" src="../javascript/script.js"></script>
	<script type="text/javascript" src="../aos/dist/aos.js"></script>

</body>
</html>