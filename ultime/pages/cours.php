<?php
require_once("../connexiondb.php");

if(isset($_GET['nomC'])&& isset($_GET['semestre'])){
   $semestreC = $_GET['semestre'];
   $nomc = $_GET['nomC'];
   if ($semestreC == 'all') {
       $requete="select * from cours where nom_cour like '%$nomc%'";
       $ResultatsCours = $fgidb->query($requete);
   }else
      
      $requete="select * from cours where nom_cour like '%$nomc%' and semestre_cour like '%$semestreC%'";
      $ResultatsCours = $fgidb->query($requete);

}else{

$requete="select * from cours";
$ResultatsCours = $fgidb->query($requete);
}




?>

<!DOCTYPE html>
<html>
<head> 
	<title>Nos offres</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">

	<link rel="stylesheet" type="text/css" href="../style/styleAbout.css">
	<link rel="stylesheet" type="text/css" href="../style/cours.css">
	<link rel="stylesheet" type="text/css" href="../aos/dist/aos.css">
  <link rel="stylesheet" type="text/css" href="../accueilstyle.css">

	<script type="text/javascript" src="../javascript/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="../javascript/bootstrap.min.js"></script>
	<script type="text/javascript" src="../javascript/popper.min.js"></script>
	<script type="text/javascript" src="../javascript/all.js"></script>
	<script type="text/javascript" src="../javascript/script.js"></script>
	<script type="text/javascript" src="../aos/dist/aos.js"></script>
</head>
<body>

    <header id="home">
		   <div class="container">  
            <div class="row">
              <i id="icon" style="cursor: pointer;" class="fa fa-bars fa-2x"></i>

                <div class="col-md-3 col-xs-12 " style="display: flex; ">
                <img src="../logo.png " class="img-fluid" width="40px" height="40px">
                  <a href="index.php"> <div class="logo"><h2>FGIApp</h2></div></a>
                </div>
                <nav class="col-md-9 col-xs-12 ">
                    <ul class="ul-list">
                        <li class="list"><a class="nav-a" href="../index.php">Accueil</a></li>
                        <li class="list"><a class="nav-a" href="pages/about.php">A propos</a></li>
                        <li class="list"><a class="nav-a" href="#">Forum</a></li>
                        <li class="list"><a class="nav-a" href="pages/filiere.php">Filieres</a></li>
    
                    </ul>
                </nav>
            </div>
        </div>
	  </header>

	<div class="container">	
	<div class="panel panel-primary  margetop">
    	<div class="panel-heading">
    		Rechercher des cours...
    	</div>
    	<div class="panel-body">
    		<form method="get" action="cours.php" class="form-inline">
               <div class="form-group"> 
                 <input type="text" name="nomC" placeholder="taper le nom du cours" class="form-control" value=""> 
              </div>
              Semestre :
               <select name="semestre"  id="semestre" >
                   <option value="all">tous les niveaux </option>
                   <option value="1" >1</option>
                   <option value="2">2</option>
                   <option value="3">3</option>
                   <option value="4" >4</option>
                   <option value="5" >5</option>
                   <option value="5" >6</option>
                   <option value="5" >7</option>
                   <option value="5" >8</option>
                   <option value="5" >9</option>
                   <option value="5" >10</option>
               </select>
               <button method="get" action="cours.php" type="submit" class="btn btn-success">
                <span class="glyphicon glyphicon-search"></span>
                chercher...
               </button> 
               &nbsp &nbsp
            </form>
    		
    	</div>
    </div>
    <div class="panel panel-primary ">
    	<div class="panel-heading">
    		resultat Recherche des cours..
    	</div>
    	<div class="panel-body">
    		<table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Id cours</th><th>Nom du cour</th><th>Semestre</th>
                    </tr>
                </thead>
                <tbody>
                	<?php 
                	    while($cours=$ResultatsCours->fetch()){?>
                        <tr>
                        	
                          <td> <?php echo $cours['id_cour'] ?></td>
                          <td> <?php echo $cours['nom_cour'] ?></td>
                          <td> <?php echo $cours['semestre_cour'] ?></td>
                      
                        </tr>
                        <?php } ; ?>
                     


                  
                </tbody>
            </table>
    	</div>
    </div>
 </div>

 <!-- footer -->
 <section class="menu" id="menu">
			<?php include("../pages/footer.php") ?>
		</section>


</body>
</html>