<!DOCTYPE html>
<html>
<head> 
	<title>Nos offres</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">

	<link rel="stylesheet" type="text/css" href="../style/styleAbout.css">
	<link rel="stylesheet" type="text/css" href="../style/cours.css">
	<link rel="stylesheet" type="text/css" href="../aos/dist/aos.css">
	<link rel="stylesheet" type="text/css" href="../accueilstyle.css">

</head>
<body>

 <header id="home">
		<?php include("../pages/header.php") ?>
	</header>

	<div class="container">	
	<div class="panel panel-success margetop">
    	<div class="panel-heading">
    		Rechercher des notes...
    	</div>
    	<div class="panel-body">
    		<form method="get" action="filieres.php" class="form-inline">
               <div class="form-group"> 
                 <input type="text" name="nomM" placeholder="nom de la matiere.." class="form-control" value=""> 
              </div>
              niveau :
               <select name="matiere"  id="matiere" >
                   <option value="all">tous les niveaux </option>
                   <option value="1" >1</option>
                   <option value="2">2</option>
                   <option value="3">3</option>
                   <option value="4" >4</option>
                   <option value="5" >5</option>
               </select>
              Année Academique :
               <select name="Annee"  id="anne" >
               	   <option value="allA">toutes les Années </option>
                   <option value="2017_2018">2017-2018</option>
                   <option value="2018_2019" >2018-2019</option>
                   <option value="2019_2020">2019-2020</option>
               </select>
               <button type="submit" class="btn btn-success">
                <span class="glyphicon glyphicon-search"></span>
                chercher...
               </button> 
               &nbsp &nbsp
            </form>
    		
    	</div>
    </div>
    <div class="panel panel-primary ">
    	<div class="panel-heading">
    		resultat Recherche des notes..
    	</div>
    	<div class="panel-body">
    		<table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Id notes</th><th>Nom de la matierer</th><th>Niveau</th>
                    </tr>
                </thead>
                <tbody>
                        <tr>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                     <td>


                  
                </tbody>
            </table>
    	</div>
    </div>
 </div>
 <section class="menu" id="menu">
			<?php include("../pages/footer.php") ?>
		</section>




	<script type="text/javascript" src="../javascript/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="../javascript/bootstrap.min.js"></script>
	<script type="text/javascript" src="../javascript/popper.min.js"></script>
	<script type="text/javascript" src="../javascript/all.js"></script>
	<script type="text/javascript" src="../javascript/script.js"></script>
	<script type="text/javascript" src="../aos/dist/aos.js"></script>
</body>
</html>