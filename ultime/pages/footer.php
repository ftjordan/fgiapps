<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">

<div class="container">
				<div class="row text-center text-white">
					<div class="col-12 col-md-3 mb-3 mb-md-0" data-aos="fade-up">
						<div  class="shop-info">
							<i style="color: turquoise; font-size: 40px" class="far fa-clock mb-3"></i>
							<h1 class="mb-4" style="cursor: pointer;">Centre d'operation</h1>
							<p>ouverte Lundi-Vendredi  8h à 17h</p>
							<p>Samedi 8h à 14h</p>
						</div>
					</div>
					<div class="col-12 col-md-3 mb-3 mb-md-0" data-aos="fade-right">
						<div class="shop-info">
							<i style="color: turquoise; font-size: 40px" class="fas fa-book mb-3"></i>
							<h1 class="mb-4">Notre adresses</h1>
							<address>
								pk17 <br>
								Douala, Dla 
							</address>
						</div>
					</div>
					<div class="col-12 col-md-3 mb-3 mb-md-0" data-aos="fade-down">
						<div class="shop-info">
							<i style="color: turquoise; font-size: 40px" class="fas fa-mobile-alt mb-3"></i>
							<h1 class="mb-4">Apppelez</h1>
							<p>phone #: (+237) 6 90 21 16 72/ 6 55 28 75 54</p>
							<p>E-mail: FGI-UD.com</p>
						</div>
					</div>
					<div class="col-12 col-md-3 mb-3 mb-md-0" data-aos="fade-up">
						<div class="shop-info">
							<i style="color: turquoise; font-size: 40px" class="fas fa-mobile-alt mb-3"></i>
							<h1 class="mb-4">Suivez nous sur</h1>
							<p><span class="glyphicon glyphicon-facebook"></span>facebook</p>
							<p>twitter</p>
						</div>
						 <a href="#about">
                            <span class="fa fas-chevron-up"></span>
                         </a>
					</div>
				</div>
				<hr class="mt-5">
				
			</div>