<!DOCTYPE html>
<html>
<head> 
	<title>Nos offres</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
	
	<link rel="stylesheet" type="text/css" href="../style/styleAbout.css">
	<link rel="stylesheet" type="text/css" href="../aos/dist/aos.css">
	<link rel="stylesheet" type="text/css" href="../accueilstyle.css">

	<script type="text/javascript" src="../javascript/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="../javascript/bootstrap.min.js"></script>
	<script type="text/javascript" src="../javascript/popper.min.js"></script>
	<script type="text/javascript" src="../javascript/all.js"></script>
	<script type="text/javascript" src="../javascript/script.js"></script>

	<script type="text/javascript" src="../aos/dist/aos.js"></script>
</head>
<body>

 <header>
		<?php include("../pages/header.php") ?>
	</header>
	<section class="about" id="about">
			<div class="container">
				<div class="row align-items-lg-center">
					<div class="col-12 col-md-6 text-center text-md-center " data-aos="fade-right">
						<div class="section-heading mb-3">
							<h1 class="display-4">Robotique Industriel</h1>
						</div>
						<p>c'est l'avenir de demain</p>
					</div>
					<div class="col-12 col-md-6" data-aos="fade-left">
						<img class="img-fluid" src="../images/roi.jpg" alt="">
					</div>
				</div>
				<div class="row align-items-lg-center">
					<div class="col-12 col-md-6 text-center text-md-center " data-aos="fade-right">
						<div class="section-heading mb-3">
							<h1 class="display-4">Telecom/ Reseaux</h1>
						</div>
						<p> Devenir ingenieur en genie telecom reseau a la FGI</p>
					</div>
					<div class="col-12 col-md-6" data-aos="fade-left">
						<img class="img-fluid" src="../images/telecom.jpg" alt="">
					</div>
				</div>
				<div class="row align-items-lg-center">
					<div class="col-12 col-md-6 text-center text-md-center " data-aos="fade-right">
						<div class="section-heading mb-3">
							<h1 class="display-4">genie Informatique</h1>
						</div>
						<p> le genie informatique c'est le monde de demain</p>
					</div>
					<div class="col-12 col-md-6" data-aos="fade-left">
						<img class="img-fluid" src="../images/reseau.jpg" alt="">
					</div>
				</div>
				<div class="row align-items-lg-center">
					<div class="col-12 col-md-6 text-center text-md-center " data-aos="fade-right">
						<div class="section-heading mb-3">
							<h1 class="display-4">le genie des procedes</h1>
						</div>
						<p> le genie civil a la fgi est ....</p>
					</div>
					<div class="col-12 col-md-6" data-aos="fade-left">
						<img class="img-fluid" src="../images/procede.jpg" alt="">
					</div>
				</div>
			</div>
		</section>
		
		<section class="menu" id="menu">
			<?php include("../pages/footer.php") ?>
		</section>
	</main>


</body>
</html>